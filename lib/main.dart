import 'package:flutter/material.dart';
import 'chewie_list_item.dart';
import 'package:video_player/video_player.dart';




void main() {
  runApp(MyApp());
}



class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Video Controller Test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Video Controller Test'),
    );
  }
}



class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}




class _MyHomePageState extends State<MyHomePage> {

  ChewieListItem _portraitVidController;
  ChewieListItem _landscapeVidController;
  @override
  void initState() {
    super.initState();
    //Portrait
    _portraitVidController = ChewieListItem(
      // videoPlayerController: VideoPlayerController.network('https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4'),
      videoPlayerController: VideoPlayerController.network('https://assets.mixkit.co/videos/preview/mixkit-portrait-of-a-fashion-woman-with-silver-makeup-39875-large.mp4'),
    );

    //Landscape
    _landscapeVidController = ChewieListItem(
      videoPlayerController: VideoPlayerController.network('https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4'),
      // videoPlayerController: VideoPlayerController.network('https://assets.mixkit.co/videos/preview/mixkit-portrait-of-a-fashion-woman-with-silver-makeup-39875-large.mp4'),
    );
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //PORTRAIT
            Stack(
              children: [
                Container(
                  height: 200,
                  width: 200,
                  color: Colors.red,
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                    child: FittedBox(
                      fit: BoxFit.cover,
                      alignment: Alignment.center,
                      child: _portraitVidController,
                    ),
                  ),
                ),
                Container(
                  width: 200,
                  height: 200,
                  // color: Colors.blue,
                  child: TextButton(
                    onPressed: () {
                      _portraitVidController.enterFS();
                    },
                    child: null,),
                ),

              ],
            ),

            SizedBox.fromSize(size: Size(0, 20),),

            //LANDSCAOE
            Stack(
              children: [
                Container(
                  height: 200,
                  width: 200,
                  color: Colors.red,
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                    child: FittedBox(
                      fit: BoxFit.cover,
                      alignment: Alignment.center,
                      child: _landscapeVidController,
                    ),
                  ),
                ),
                Container(
                  width: 200,
                  height: 200,
                  // color: Colors.blue,
                  child: TextButton(
                      onPressed: () {
                        _landscapeVidController.enterFS();
                      },
                      child: null),
                ),

              ],
            ),


            ],
        ),
      ),
    );
  }
}
