import 'package:flutter/material.dart';
import 'package:chewie/chewie.dart';
import 'package:video_player/video_player.dart';


class ChewieListItem extends StatefulWidget {
  ChewieController _chewieController;

  final VideoPlayerController videoPlayerController;
  final bool looping;

  enterFS() {
    _chewieController.enterFullScreen();
  }

  ChewieListItem({
    @required this.videoPlayerController,
    this.looping,
    Key key,
  }): super(key: key);



  @override
  _ChewieListItemState createState() => _ChewieListItemState();
}

class _ChewieListItemState extends State<ChewieListItem> {

  @override
  void initState() {
    super.initState();
    widget._chewieController = ChewieController(
      videoPlayerController: widget.videoPlayerController,
      autoInitialize: true,
      looping: widget.looping,
      showControls: true,
      // aspectRatio: 16/9,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Chewie(
        controller: widget._chewieController,
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    widget.videoPlayerController.dispose();
    widget._chewieController.dispose();
  }
}
